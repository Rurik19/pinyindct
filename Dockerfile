FROM node
WORKDIR /app
COPY package.json .
RUN npm i
COPY . .
RUN npm run build

FROM caddy:alpine
COPY Caddyfile /etc/caddy/Caddyfile
COPY --from=0 /app/dist /usr/share/caddy

EXPOSE 80