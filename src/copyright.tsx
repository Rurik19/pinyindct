import { ReactElement } from "react";
import { strings } from "./strings";
import { author } from '../package.json'

export const Copyright = ():ReactElement => 
<div className="cpr">{strings.version} {import.meta.env.VITE_REACT_APP_VERSION} {strings.cpr}{author.name} {strings.mail}{author.email}</div>