import { ReactElement } from "react";
import { useStateContext } from "./store";
import { Form } from "react-bootstrap";
import { ActionType } from "./reducer";
import { strings } from "./strings";
import { FormatString } from "./utils";

export const Params = (): ReactElement => {

    const { state, dispatch } = useStateContext();

    const pausedispatcher = (e: React.ChangeEvent<HTMLInputElement>) =>
        dispatch({ type: ActionType.setPause, payload:  Number(e.target.value)})
    const countdispatcher = (e: React.ChangeEvent<HTMLInputElement>) =>
        dispatch({ type: ActionType.setCount, payload:  Number(e.target.value)})

    return <>
         <Form.Label>{strings.sylCount} {state.sylCount}</Form.Label>
          <Form.Range value={state.sylCount} min={5} max={50} step={5} onChange={countdispatcher}/>
          <Form.Label>{ FormatString( strings.pauseFormat , state.sylPause!.toString() )}</Form.Label>
          <Form.Range value={state.sylPause} min={1} max={10} step={1} onChange={pausedispatcher}/>
    </>

}