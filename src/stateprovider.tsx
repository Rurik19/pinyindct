import { ReactElement, useReducer } from "react";
import { reducer } from "./reducer";
import { AppContext, defaultState, IState } from "./store";

export const StateProvider = ({ children }: { children: ReactElement }) => {
    const [state, dispatch] = useReducer(reducer, defaultState as IState);
    return <AppContext.Provider value={{ state, dispatch }} children={children} />
}