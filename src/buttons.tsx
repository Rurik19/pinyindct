import { Button } from "react-bootstrap";
import { useStateContext } from "./store";
import { Outlined } from "./utils";
import { ActionType, ToggleType } from "./reducer";
import { strings } from "./strings";
import { BtnColor, SylPart } from "./types";

export interface IBtnSerProps {
  source: SylPart[],
  color: BtnColor,
  toggle: ToggleType
}

export const ButtonSet = (props: IBtnSerProps) => {
    const { state, dispatch } = useStateContext()
    const { source, color, toggle } = props
    return <>
          <Button variant={ state.allEnabled!(toggle) ? color : Outlined(color) }
                  onClick= {()=>dispatch({type: ActionType.toggleAll, payload: toggle})}>
            {state.allEnabled!(toggle) ? strings.unselectAll : strings.selectAll}
          </Button>
           { source.map( (part, i) => <Button
                                          variant={ state.isEnabled!(toggle, part.index) ? color : Outlined(color) }
                                          key={i}
                                          onClick={()=>dispatch({
                                            type: ActionType.toggleOne,
                                            payload: { type: toggle, part: part }
                                            })}>
                                          {part.caption}
                                      </Button>
                            )}
   </>
}