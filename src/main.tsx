import  * as React  from 'react'
import * as ReactDOM from 'react-dom/client'
import App from './App'
import './index.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import { StateProvider } from './stateprovider';

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <StateProvider>
        <App />
    </StateProvider>
  </React.StrictMode>,
)
