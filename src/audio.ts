export const playFile = (filename: string) => {
    const audio = new Audio(`/src/assets/audio/${filename}`)
    audio.play()
}

// export const playTone = async (toneName: string) => {
//     const audio = new Audio(`/src/assets/audio/${toneName}.mp3`)
//     await audio.play()
// }

export const playTone = (index: string) => {
    getAudio(index).play()
  }

export const getAudioPath = (index: string):string => `/audio/${index}.mp3`
export const getAudio = (index: string):HTMLAudioElement => new Audio(getAudioPath(index))