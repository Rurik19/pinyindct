import { ReactElement } from "react";
import { playTone } from "./audio";
import { Tone } from "./types";

interface IRenderTonesProps {
    tones: Tone[]
}

export const RenderTones = (props:IRenderTonesProps):ReactElement => {
    const { tones } = props
    return <div>
        { tones!.map(
        (ton, i) => { return <span key={i}
            className={`syllable tone${ton.num}`}
            onClick={() => playTone(ton.tone)}
        >
            {i + 1}{'. '}{ton.caption}{'  '}
        </span> } ) }
    </div>
}