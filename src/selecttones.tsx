import { useStateContext } from "./store"
import { Form } from "react-bootstrap";
import { strings } from "./strings";
import { ActionType } from "./reducer";

export const SelectTones = () => {
    const { state, dispatch } = useStateContext()
    const onToneChange = ( toneNo: number ) =>
         dispatch({ type:ActionType.toneSelect, payload: toneNo })
    return <>
    <Form>
      <Form.Check // prettier-ignore
        inline
        type="switch"
        id="first-switch"
        label={strings.first}
        checked = {state.isToneChecked!(1)}
        onChange = { () => onToneChange(1) }
      />
      <Form.Check // prettier-ignore
        inline
        type="switch"
        label={strings.second}
        id="second-switch"
        checked = {state.isToneChecked!(2)}
        onChange = { () => onToneChange(2) }
      />
      <Form.Check // prettier-ignore
        inline
        type="switch"
        id="third-switch"
        label={strings.third}
        checked = {state.isToneChecked!(3)}
        onChange = { () => onToneChange(3) }
      />
      <Form.Check // prettier-ignore
        inline
            type="switch"
        label={strings.fourth}
        id="fourth-switch"
        checked = {state.isToneChecked!(4)}
        onChange = { () => onToneChange(4) }
      />
      <Form.Check // prettier-ignore
        inline
            type="switch"
        label={strings.zeroth}
        id="zeroth-switch"
        checked = {state.isToneChecked!(5)}
        onChange = { () => onToneChange(5) }
      />
    </Form>
    </>
}