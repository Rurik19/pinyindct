export type proc = (caption: string) => void;
export type justproc = () => void;

export type Syllable = {
    syllable: string,
    initiale: string,
    finale: string,
    tones: string[]
}

export type SylPart = {
    caption: string,
    index: string
}

export type Tone = {
    tone: string,
    caption: string,
    num: number
}

export type Found = {
    initiales: string[],
    finales: string[],
    allInitiales: boolean,
    allfinales: boolean,
    syllables: Syllable[],
    toneS: Tone[],
    randomTones: Tone[]
}

export enum BtnColor {
    blue = "primary",
    green = "success"
}

export enum Status {params, prepare, playing, plaied, showlist}